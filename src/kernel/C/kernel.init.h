__asm__
(
    "int $25\n\t"
    "ros_init:\n\t" 
    "cli\n\t" // Clear interrupts
    "mov 0, %rax\n\t"
    "mov %rax, %ss\n\t" // Set stack segment and pointer
    "mov 0xFFFFFFFFFFFFFFFF, %rsp \n\t"
    "sti\n\t" // Restore interrupts
    "cld\n\t" // The default direction for string operations will be 'up' - incrementing address in RAM

    "mov 0x2000, %rax\n\t" // Set all segments to match where kernel is loaded
    "mov %rax, %ds\n\t"
    "mov %rax, %es\n\t"
    "mov %rax, %fs\n\t"
    "mov %rax, %gs\n\t"
    //"jmp main\n\t"

    // "push %dx\n\t"
    // "push %ax\n\t"
    // "mov 0x3F8, %dx\n\t"
    // "mov 65, %ax\n\t"
    // "out %ax, %dx\n\t"
    //"jmp main\n\t"


    
);