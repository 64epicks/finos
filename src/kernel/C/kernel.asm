	.file	"kernel.c"
	.text
#APP
	ros_init:
	cli
	mov 0, %rax
	mov %rax, %ss
	mov 0xFFFFFFFFFFFFFFFF, %rsp 
	sti
	cld
	mov 0x2000, %rax
	mov %rax, %ds
	mov %rax, %es
	mov %rax, %fs
	mov %rax, %gs
	int $0x19
	
#NO_APP
	.globl	pusha
	.type	pusha, @function
pusha:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
#APP
# 6 "basicIO.c" 1
	popq %rbp
	pushq %rax
	pushq %rbx
	pushq %rcx
	pushq %rdx
	pushq %rsi
	pushq %rdi
	pushq %rbp
	movq %rsp, %rbp
	
# 0 "" 2
#NO_APP
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	pusha, .-pusha
	.globl	popa
	.type	popa, @function
popa:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
#APP
# 21 "basicIO.c" 1
	popq %rbp
	popq %rdi
	popq %rsi
	popq %rdx
	popq %rcx
	popq %rbx
	popq %rax
	pushq %rbp
	movq %rsp, %rbp
	
# 0 "" 2
#NO_APP
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	popa, .-popa
	.globl	print
	.type	print, @function
print:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$24, %rsp
	movq	%rdi, -24(%rbp)
	movl	$0, %eax
	call	pusha
	leaq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	print, .-print
	.section	.rodata
.LC0:
	.string	"rax"
.LC1:
	.string	"rbx"
	.text
	.globl	getRegistry
	.type	getRegistry, @function
getRegistry:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	leaq	.LC0(%rip), %rax
	cmpq	%rax, -24(%rbp)
	jne	.L5
#APP
# 45 "basicIO.c" 1
	movq %rax, %rax
# 0 "" 2
#NO_APP
	movq	%rax, -8(%rbp)
	jmp	.L6
.L5:
	leaq	.LC1(%rip), %rax
	movq	%rax, -24(%rbp)
#APP
# 49 "basicIO.c" 1
	movq %rbx, %rax
# 0 "" 2
#NO_APP
	movq	%rax, -8(%rbp)
.L6:
	movq	-8(%rbp), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	getRegistry, .-getRegistry
	.globl	getPort
	.type	getPort, @function
getPort:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, %eax
	movb	%al, -4(%rbp)
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	getPort, .-getPort
	.section	.rodata
.LC2:
	.string	"Hello World!"
	.text
	.globl	main
	.type	main, @function
main:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	leaq	.LC2(%rip), %rdi
	call	print
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	main, .-main
	.ident	"GCC: (GNU) 8.2.1 20181127"
	.section	.note.GNU-stack,"",@progbits
