BITS 16
org 0 ; Offset we're loaded at
%DEFINE LOWA_VERSION     '0.1'
%DEFINE LOWA_API_VERSION '0.1'

basic_api_callvectors:
    jmp kernel_init  ; 0x0000
    jmp print_string ; 0x0003
    jmp read_sectors ; 0x0006
    jmp reboot       ; 0x0009
    jmp error        ; 0x000C
    jmp get_root_dir ; 0x000F

kernel_init:
    cli
    mov ax, 0
	mov ss, ax		; Set stack segment and pointer
	mov sp, 0xFFFF
    sti
    mov ax, 0x0100		
	mov ds, ax			
	mov es, ax			
	mov fs, ax			
	mov gs, ax

    mov [bootdev], dl

    mov si, startMsg
    call lowa_segment:lowa_print_string
read_root_dir:
    mov ax, 19
    mov cl, 14

    mov si, buffer			; Set ES:BX to point to our buffer (see end of code)
	mov bx, ds
	mov es, bx
	mov bx, si
    call lowa_segment:lowa_read_sectors
	jc lowaError
seach_CLinker:
    mov ax, ds			; Root dir is now in [buffer]
	mov es, ax			; Set DI to this info
	mov di, buffer

	mov cx, 224	; Search all (224) entries
	mov ax, 32			; Searching at offset 32 = FIRST LABEL
	add di, ax
	xor dx, dx ; Dx acts as counter
.REPEAT:
	inc dx
	mov si, CLinkerName
	push dx ; We're going to use dx in the inner loop
	push ax
	xor dx, dx
.STRING_REP:
	inc dx
	lodsb ; Name string = ah, file = al
	mov ah, al
	xchg si, di
	lodsb

	push ax
	mov ah, 0Eh
	int 0x10
	pop ax

	xchg si, di
	cmp ah, al
	jne .DONE
	cmp dx, 11
	je load_CLinker
	jmp .STRING_REP
.DONE:
	pop ax
	pop dx

	cmp dx, cx
	je .NO_CLINKER

	add ax, 32 ; Next entry

	mov di, buffer
	add di, ax

	jmp .REPEAT
.NO_CLINKER:
	mov si, no_file
	call lowa_segment:lowa_print_string
	jmp $
load_CLinker:
    jmp $

lowaError:
	mov si, lowaErrorMsg
	call lowa_segment:lowa_print_string
	mov si, error_message
	call lowa_segment:lowa_print_string
	mov si, lowaErrorResetMsg
	call lowa_segment:lowa_print_string
	call lowa_segment:lowa_reboot

; LOCAL VARIABLES
drive_parameters_set db 0
error_message dw 0 ; Memory address for error
;.DISK VARIABLES
bootdev db 0
SectorsPerTrack dw 0
SectorsPerHead dw 0
; .STRINGS
startMsg db 'Loading LOWA version ', LOWA_VERSION, ' (API version ', LOWA_API_VERSION, ')', 0x0D, 0x0A, 0
CLinkerName db "CLINKER BIN"
no_file db 'clinker.bin not found!', 0x0D, 0x0A, 'System halt', 0
lowaErrorMsg db 'An error has occured', 0x0D, 0x0A, 0
lowaErrorResetMsg db 'The system have to reset', 0x0D, 0x0A, 'Press any key to reboot...', 0
; API
api_start:
%INCLUDE "../API/LOWA/LOWA.asm"
%INCLUDE "../API/LOWA/LOWA.inc"

buffer:
