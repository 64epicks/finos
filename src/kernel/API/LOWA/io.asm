print_string:
    pusha
    mov ah, 0Eh
.REPEAT:
    lodsb
    cmp al, 0
    je .DONE
    int 10h
    jmp short .REPEAT
.DONE:
    popa
    ret