print_string:
    pusha
    mov ah, 0Eh
.REPEAT:
    lodsb
    cmp al, 0
    je .DONE
    int 10h
    jmp short .REPEAT
.DONE:
    popa
    ret
DISK_SET_ERROR:
    mov ax, disk_reset_error
    mov dx, 0 ; 0 for not fatal
    call basic_segment:lowa_error
    popa
    ret
DISK_READ_ERROR:
    mov ax, disk_read_error
    mov dx, 0 ; 0 for not fatal
    call basic_segment:lowa_error
    popa
    ret

reboot:
    mov ax, 0
    int 16h
    mov ax, 0
    int 19h

%INCLUDE "../API/BASIC/lowa_error.asm"
%INCLUDE "../API/BASIC/disk.asm"
