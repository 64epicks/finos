error:
    pusha
    stc ; Set carry flag for error
    cmp ax, 0
    jne .DISK_RESET_ERROR
.DISK_READ_ERROR:
    mov si, .DISK_ERROR_MSG
    mov [error_message], si
    jmp .DONE
    .DISK_ERROR_MSG db 'Fatal disk error: Could not read disk! DISK_READ_ERROR', 0
.DISK_RESET_ERROR:
    mov si, .DISK_SET_ERROR_MSG
    mov [error_message], si
    jmp .DONE
    .DISK_SET_ERROR_MSG db 'Fatal disk error: Could not reset disk! DISK_RESET_ERROR', 0
.DONE:
    cmp dx, 1
    je .FATAL
    popa
    ret
.FATAL:
    call lowa_segment:lowa_print_string
    mov si, .FATAL_MSG
    call lowa_segment:lowa_print_string
    jmp lowa_segment:lowa_reboot
.FATAL_MSG db 0x0D, 0x0A, 'The system encountered an error and has to reboot.', 0x0D, 0x0A, 'Press any key to reboot...', 0

DISK_SET_ERROR:
    mov ax, disk_reset_error
    mov dx, 1 ; 1 for not fatal
    jmp lowa_segment:lowa_error
DISK_READ_ERROR:
    mov ax, disk_read_error
    mov dx, 1 ; 1 for not fatal
    jmp lowa_segment:lowa_error