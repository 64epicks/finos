lowa_segment equ       0x0100
lowa_print_string equ  0x0003 ; INPUT: si = memory address of null terminated string; OUTPUT: NONE
lowa_read_sectors equ  0x0006 ; INPUT: ax = first sector, cl = number of sectors to read, es:bx = memory address to store data
lowa_reboot equ        0x0009 ; NO INPUT
lowa_error equ         0x000C ; INPUT: ax = error type, dx = fatal(0x0001) or not(0x0000). OUTPUT = CF set, error_message = error description
lowa_get_root_dir equ  0x000F ; INPUT: es:bx place to store root dir

; ERROR TYPES:
disk_read_error  dw 0
disk_reset_error dw 1