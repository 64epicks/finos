get_root_dir:
    pusha
    mov cx, 1
    mov al, 14
    call lowa_segment:lowa_read_sectors
    popa
    ret

read_sectors:
    pusha
    mov al, BYTE [drive_parameters_set] ; Check if we got the drive parameters
    cmp al, 0
    je .DRIVE_PARAM
.START:
    popa
    pusha
    mov di, 6 ; Five tries before error
.LOOP:
    dec di
    push cx
    call PRIVATE_l2hts
    pop ax
    mov ah, 2
    int 0x13
    jnc .DONE
    call PRIVATE_reset_disk
    jc DISK_SET_ERROR
    cmp di, 0
    jne .LOOP
    jmp DISK_READ_ERROR
.DONE:
    popa
    ret
.DRIVE_PARAM:
    call PRIVATE_get_drive_parameters
    jmp .START
PRIVATE_l2hts:		; Calculate head, track and sector settings for int 13h
      		; IN: logical sector in AX, OUT: correct registers for int 13h
	push bx
	push ax

	mov bx, ax			; Save logical sector

	mov dx, 0			; First the sector
	div word [SectorsPerTrack]
	add dl, 01h			; Physical sectors start at 1
	mov cl, dl			; Sectors belong in CL for int 13h
	mov ax, bx

	mov dx, 0			; Now calculate the head
	div word [SectorsPerTrack]
	mov dx, 0
	div word [SectorsPerHead]
	mov dh, dl			; Head/side
	mov ch, al			; Track

	pop ax
	pop bx

	mov dl, byte [bootdev]		; Set correct device

	ret

PRIVATE_reset_disk:
    push ax
    push dx
    mov ax, 0
    mov dl, byte [bootdev]
    stc
    int 0x13
    pop dx
    pop ax
    ret

PRIVATE_get_drive_parameters:
    pusha
    mov ah, 8
    int 0x13
    jc DISK_READ_ERROR
    and ax, 0x3F
    mov [SectorsPerTrack], cx
    movzx dx, dh			; Maximum head number
	add dx, 1			; Head numbers start at 0 - add 1 for total
	mov [SectorsPerHead], dx
    mov al, 1
    mov [drive_parameters_set], al
    popa
    ret