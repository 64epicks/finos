#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <string.h>
#include <limits>

using namespace std;

// char* intToByteArray(int64_t value);


int main(int argc, char** argv)
{
    if (argc == 2)
    {
        char* path = argv[1];
        std::ofstream ofs(path);
        char sizeChar[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
        char size[8] = { 0, 0, 0, 0, 0, 0, 127, 127 };
        int64_t sizeInt = 32639;
        char nxtSector[8] = { 0, 0, 0, 0, 0, 0, 127, 126};
        char atEnd[8] = { 0, 0, 0, 0, 0, 0, 0, 33}; // AT starts with 32 sectors. 32 + 1(Reserved sector) = 33.
        
        char null[1] = { 0 };

        for (int i = 0; i < 512; i++) ofs.write(null, sizeof(char));
        //for (int i = 0; i < 512; i++) fwrite(null, 1, sizeof(unsigned char), file);
        #pragma region Reserved
        for (int i = 0; i < 16; i++) ofs.write(null, sizeof(char));
        
        ofs.write(size, sizeof(char)*8);
        ofs.write(nxtSector, sizeof(char)*8);
        ofs.write(atEnd, sizeof(char)*8);

        for(int i = 0; i < 462; i++) ofs.write(null, sizeof(char));
        #pragma endregion
        #pragma region Write body
        //for(size_t i = 0; i < 512*sizeInt; i++) ofs.write(null, sizeof(char));
        #pragma endregion
    } 
}
// char* intToByteArray(int64_t value)
// {
//     char retdata[8];
//     malloc(8);
//     retdata[0] = (value >> 56) & 0xFF;
//     retdata[1] = (value >> 48) & 0xFF;
//     retdata[2] = (value >> 40) & 0xFF;
//     retdata[3] = (value >> 32) & 0xFF;
//     retdata[4] = (value >> 24) & 0xFF;
//     retdata[5] = (value >> 16) & 0xFF;
//     retdata[6] = (value >> 8) & 0xFF;
//     retdata[7] = value & 0xFF;

//     return retdata;
// }