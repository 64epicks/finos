#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <string.h>
#include <limits>
#include <vector>
#include <inttypes.h>
#include <math.h>
#include <memory>

using namespace std;

int64_t S64(const char *s) {
  int64_t i;
  char c ;
  int scanned = sscanf(s, "%" SCNd64 "%c", &i, &c);
  if (scanned == 1) return i;
  if (scanned > 1) {
    // TBD about extra data found
    return i;
    }
  // TBD failed to scan;  
  return 0;  
}

const unsigned char* int64ToChar(uint64_t interger)
{
    uint64_t max = 9.22337203685e+18;
    vector<unsigned char> returnVal;
    for(int i = 7; i >= 0; i--)
    {
        returnVal.push_back(0);
        for(int t = 8; t > 0; t--)
        {
            if (interger >= max)
            {
                returnVal[i] += pow(2, t) / 2;
            }
            max = max / 2;
        }
    }
}

int main(int argc, char** argv)
{
    if (argc == 3)
    {
        char* path = argv[1];
        char* filePath = argv[2];
        ifstream diskifs(path);
        ifstream fileifs(filePath);

        fstream diskffs(path);
        
        string diskcontents((std::istreambuf_iterator<char>(diskifs)), std::istreambuf_iterator<char>());
        string filecontents((std::istreambuf_iterator<char>(fileifs)), std::istreambuf_iterator<char>());

        uint64_t flSize = (uint64_t)filecontents.size()/512;
        double flSizeDD = (double)filecontents.size()/512;
        if (flSizeDD > flSize) flSize++;

        if (filecontents.size() != flSize*512) for(int i = 0; i < flSize*512 - filecontents.size(); i++) filecontents += (char)0;

        uint64_t frstStart = S64(diskcontents.substr(512, 8).c_str());
        uint64_t frstLength = S64(diskcontents.substr(520, 8).c_str());

        uint64_t driveSize = S64(diskcontents.substr(528, 8).c_str());

        if (frstStart != 0)
        {
            uint64_t fileStart;
            if (driveSize > 0xFFFF) fileStart = 0xFFFF - flSize;
            else fileStart = driveSize - flSize;

            diskffs.seekp(fileStart*512, std::ios_base::beg);
            diskffs.write(filecontents.c_str(), filecontents.size());
        }
        else
        {

        }
    }
}