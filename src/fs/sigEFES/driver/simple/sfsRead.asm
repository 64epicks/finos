; Reads first file into memory at es:bx
; cx will be 1 if it could not load the file
sfs_read_first_file:


; Read cl sectors from ax into memory at es:bx
sfs_read_sector:
.MAIN:
    pusha
    mov di, 5 ; Five tries before aborting
.LOOP:
    call l2hts
    
    mov ah, 2
    mov al, cl

    int 13h
    jnc .END
    mov ah, 0
    int 13h
    jnc .LOOP
    int 16h
    int 19h
.END:
    popa
    ret

l2hts:			; Calculate head, track and sector settings for int 13h		; IN: logical sector in AX, OUT: correct registers for int 13h
	push bx
	push ax

	mov bx, ax			; Save logical sector

	mov dx, 0			; First the sector
	div word [SectorsPerTrack]
	add dl, 01h			; Physical sectors start at 1
	mov cl, dl			; Sectors belong in CL for int 13h
	mov ax, bx

	mov dx, 0			; Now calculate the head
	div word [SectorsPerTrack]
	mov dx, 0
	div word [Sides]
	mov dh, dl			; Head/side
	mov ch, al			; Track

	pop ax
	pop bx

	mov dl, byte [bootdev]		; Set correct device

	ret

resError db 'Could not reset drive...', 0x0D, 0x0A, 0x00
readError db 'Could not read drive...', 0x0D, 0x0A, 0x00
