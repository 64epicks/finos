# SigEFES structure
<img src="img/drive.png" alt="drawing" width="200"/>
*not to scale

## Notes
---
The driver should write bottom up when writing to the data sectors. This is to be able to dynamically resize the allocation table. So the first entry is at the bottom of data. An exeption is the first file. Which should not be written under 0xFFFF to maintain compatability with 16 bit bootloaders trying to load the first file. And the sectors should be written downwards.

## References
---
More detailed explanations can be found at their declarations
>Sectors are always referenced in 64-bit values (or 8 bytes).
>
>Fs = Start of the first file in the data sectors
>
>Fe = End of first file
>
>Dz = Total size of partition in sectors
>
>De = Last sector of data before the allocation table

## MBR, 1 sector
---
512 bytes that the BIOS searches for an OS.
## Reserved, 1 sector
---
The number 'X' represents how many bytes the variable takes up on the disk, '[SOMEVAR]' is how this value is referenced later. All entries are in the same order on the disk.
> 8 [Fs]: Start of the first file in the data sectors. This is here to help bootloaders.
>
> 8 [Fe]: Length of first file in sectors. So if Fs is 2 and Fe is 3 that means that the first file is in sectors 2, 3, and 4.
>
> 8 [Dz]: Total size of partition in sectors.
>
> 8 [Ns]: Next free sector in data. This is here to make writing faster.
>
> 8 [De]: Last sector of data before the allocation table (because the data sectors go from bottom up this is the last sector relative to the writing direction). This is the variable which decides the size of the allocation table and the data sectors.

## Allocation table, dynamically allocated
---
Same rules as in reserved.

<b>For each entry:</b>
>1 [Nz]:File name size.
>
>Nz [Fn]: File name.
>
>8 [Fz]: File size in sectors.
>
>8 [Fsr]: First sector of file.

## Data sectors, dynamically allocated
---
The structure here is organized by the allocation table. One should not write/read anything from here without checking the allocation table. THIS IS THE REASON WHY THIS FILE SYSTEM IS INCREDIBLY SLOW, AS YOU HAVE TO SEARCH THE WHOLE ALLOCATION TABLE BEFORE WRITING. I'M HOWEVER WORKING ON A FIX FOR THAT

The last 8 bytes in the file sector points to the next sector of the file. If it's 0 then the file is complete. The first file does not have this.