
if test "`whoami`" != "root" ; then
	echo "You must be logged in as root to build (for loopback mounting)"
	echo "Enter 'su' or 'sudo bash' to switch to root"
	exit
fi

if [ ! -e flp/finOS.flp ]
then
	echo ">>> Creating new floppy image..."
	#fallocate -l 128M flp/finOS.flp
	mkdosfs -C flp/finOS.flp 1440 || exit
fi
echo ">>> Assembling bootloader..."

nasm -O0 -w+orphan-labels -f bin -o src/bootloader/bootloader.bin src/bootloader/bootloader.asm || exit


echo ">>> Assembling LOWA..."

cd src/kernel/LOWA
	nasm -O0 -w+orphan-labels -f bin -o LOWA.bin LOWA.asm || exit
cd ../../../
# cd src/kernel/C
# 	gcc -nostdinc -o ../kernel.bin kernel.c -m64 || exit
# cd ..
# cd ..
# cd ..

echo ">>> Assembling programs..."

# cd src/programs

# for i in *.asm
# do
# 	echo "$i >> `basename $i .asm`.bin"
# 	nasm -O0 -w+orphan-labels -f bin $i -o `basename $i .asm`.bin || exit
# done
# for i in *.c
# do
# 	echo "$i >> `basename $i .c`.bin"
# 	gcc -o `basename $i .c`.bin $i
# done

# cd ..
# cd ..


echo ">>> Adding bootloader to floppy image..."

dd status=noxfer conv=notrunc if=src/bootloader/bootloader.bin of=flp/finOS.flp || exit


echo ">>> Copying kernel and programs to disk..."

rm -rf tmp-loop

mkdir tmp-loop 

mount flp/finOS.flp tmp-loop/ || exit

cp src/kernel/LOWA/LOWA.bin tmp-loop/

#cp src/programs/* tmp-loop

sleep 0.2

echo ">>> Unmounting loopback floppy..."

umount tmp-loop || exit

rm -rf tmp-loop


echo ">>> Creating CD-ROM ISO image..."

rm -f iso/finOS.iso
mkisofs -quiet -V 'finOS' -input-charset iso8859-1 -o iso/finOS.iso -b finOS.flp flp/ || exit

echo '>>> Done!'
