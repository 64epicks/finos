BITS 16

jmp short bootloader_start
nop

    OEM_STRING          db "FINDUSos"
    SectorSize          dw 0x0200
    SectorsPerCluster   db 0x08
    ReservedSectors     dw 0x0020
    NumberOfFATs        db 0x02
    MaxRootEntries      dw 0x0000
    NumberOfSectors     dw 0x0000
    MediaDescriptor     db 0xF8
    SectorsPerFAT       dw 0x0000
    SectorsPerTrack     dw 0x003D
    SectorsPerHead      dw 0x0002
    HiddenSectors       dd 0x00000000
    TotalSectors        dd 0x00FE3B1F
    BigSectorsPerFAT    dd 0x00000778
    Flags               dw 0x0000
    FSVersion           dw 0x0000
    RootDirectoryStart  dd 0x00000002
    FSInfoSector        dw 0x0001
    BackupBootSector    dw 0x0006

    TIMES 12 DB 0

    DriveNumber         db 0x00
    ReservedByte        db 0x00
    Signature           db 0x29
    VolumeID            dd 0xFFFFFFFF
    VolumeLabel         db "FINDUSosPRT"
    SystemID            db "FAT32   "

bootloader_start:
    cli
    mov ax, 0x07C0 ; Set segment registers
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov ax, 0x0000 ; Set stack
    mov ss, ax
    mov sp, 0xFFFF
    sti

    ; Get drive parameters

    mov ah, 8   ; Get drive parameters
	int 13h
    jc disk_error
    and cx, 3Fh
    mov [SectorsPerTrack], cx
    movzx dx, dh		; Maximum head number
	add dx, 1			; Head numbers start at 0 - add 1 for total
	mov [SectorsPerHead], dx

    mov al, BYTE [NumberOfFATs] ; Calculate datastart
    mul WORD [BigSectorsPerFAT]
    add ax, WORD [ReservedSectors]
    mov WORD [dataStart], ax

    mov cx, WORD[RootDirectoryStart]
    mov al, 0x01
    call ClusterLBA
    mov bx, buffer
    call read_sectors

    ; Read first file
    mov cx, WORD [buffer + 0x0020 + 0x001A]
    ;mov WORD [frstCluster], dx

    ; Read first file cluster into 0x0100:0x0000
    mov ax, 0x0100
    mov es, ax
    mov bx, 0

    mov ax, 0x01
    call ClusterLBA
    call read_sectors

    mov dl, BYTE [bootdev] ; Pass boot device to kernel
    jmp 0x1000:0x0000 ; Execute


; Reads al sectors from cx to memory at es:bx
read_sectors:
    pusha
    mov dh, 6 ; Five tries before error
.LOOP:
    dec dh
    push ax
    mov ax, cx
    call l2hts
    pop ax
    mov ah, 2
    int 0x13
    jnc .DONE
    call reset_disk
    jc disk_error
    cmp dh, 0
    jne .LOOP
    mov si, disk_read_error
    call print_string
    jmp reboot
.DONE:
    popa
    ret
reset_disk:
    push ax
    push dx
    mov ax, 0
    mov dl, byte [bootdev]
    stc
    int 0x13
    pop dx
    pop ax
    ret
disk_error:
    mov si, fdisk_error
    call print_string ; We don't need to jump to reboot since it's right under us ;)
reboot:
    mov ax, 0
    int 16h
    mov ax, 0
    int 19h
print_string:
    pusha
    mov ah, 0Eh
.REPEAT:
    lodsb
    cmp al, 0
    je .DONE
    int 10h

    push ax ; BIOS wait
    mov ah, 86h
    int 15h
    pop ax

    jmp short .REPEAT
.DONE:
    popa
    ret
l2hts:			; Calculate head, track and sector settings for int 13h		; IN: logical sector in AX, OUT: correct registers for int 13h
	push bx
	push ax

	mov bx, ax			; Save logical sector

	mov dx, 0			; First the sector
	div word [SectorsPerTrack]
	add dl, 01h			; Physical sectors start at 1
	mov cl, dl			; Sectors belong in CL for int 13h
	mov ax, bx

	mov dx, 0			; Now calculate the head
	div word [SectorsPerTrack]
	mov dx, 0
	div word [SectorsPerHead]
	mov dh, dl			; Head/side
	mov ch, al			; Track

	pop ax
	pop bx

	mov dl, byte [bootdev]		; Set correct device

	ret
    
; FileStartSector = ((cx − 2) * SectorsPerCluster(0x08))
; Returns: cx = first sector, al = sectors to read
ClusterLBA:
    push dx

    push ax
    mov ax, cx
    sub ax, 0x0002 ; zero base cluster number
    xor dx, dx
    mov dl, BYTE [SectorsPerCluster]  ; convert byte to word
    mul dx
    add ax, WORD [dataStart] ; base data sector
    mov cx, ax
    pop ax

    mul dx

    pop dx
    ret

; VARIABLES

bootdev db 0
dataStart dw 0

frstCluster dw 0

; STRINGS

disk_read_error db 'Could not read disk!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0
fdisk_error db 'Fatal disk error!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0

times 510-($-$$) db 0
dw 0xAA55

buffer: