BITS 16
jmp short bootloader_start
nop
;BEWARE THIS BOOTLOADER ONLY WORKS IF THE FIRST FILE IS WRITTEN IN SECTORS WITH 16-BIT VALUES AND A MAX LENGTH OF 255 SECTORS

OEMLabel		db "RAWBOOT "	; Disk label
BytesPerSector		dw 512		; Bytes per sector
SectorsPerCluster	db 1		; Sectors per cluster
ReservedForBoot		dw 1		; Reserved sectors for boot record
LogicalSectors		dw 2880		; Number of logical sectors
MediumByte		db 0F0h		; Medium descriptor byte
SectorsPerTrack		dw 18		; Sectors per track (36/cylinder)
Sides			dw 2		; Number of sides/heads
HiddenSectors		dd 0		; Number of hidden sectors
LargeSectors		dd 0		; Number of LBA sectors
DriveNo			dw 0		; Drive No: 0
Signature		db 41		; Drive signature: 41 for floppy
VolumeID		dd 00000000h	; Volume ID: any number
VolumeLabel		db "RAWOS      "; Volume Label: any 11 chars
FileSystem		db "sigEFEF "	; File system type: don't change!

bootloader_start:
    mov ax, 07C0h			; Set up 4K of stack space above buffer
	add ax, 544			; 8k buffer = 512 paragraphs + 32 paragraphs (loader)
	cli				; Disable interrupts while changing stack
	mov ss, ax
	mov sp, 4096
	sti				; Restore interrupts

	mov ax, 07C0h			; Set data segment to where we're loaded
	mov ds, ax

    mov [boodev], dl

    mov ah, 8			; Get drive parameters
	int 13h
    jc disk_error
    and cx, 3Fh
    mov [SectorsPerTrack], cx
    movzx dx, dh			; Maximum head number
	add dx, 1			; Head numbers start at 0 - add 1 for total
	mov [Sides], dx

    mov si, bootMsg
    call print_string
read_reserved:
    mov si, buffer
    mov bx, si
    mov es, bx
    mov bx, si

    mov cx, 1
    mov al, 1
    call read_sectors
parse: ; Get start and length of the first file
    push di
    push ax
    mov di, buffer

    mov ax, word [es:di + 5] ; Get start of file
    cmp ax, 0
    je no_kernel
    mov [file_start], ax

    mov al, byte [es:di + 14] ; Get length of file
    mov [file_length], al

    pop ax
    pop di
read_kernel:
    mov ax, 2000h ; Segment where we'll load the kernel
    mov es, ax
    mov bx, 0

    mov cx, word [file_start]
    mov al, byte [file_length]

    call read_sectors

    mov dl, byte [bootdev]
    
    jmp 2000h:0000h

; Reads al sectors from cx to memory at es:bx
read_sectors:
    pusha
    mov dh, 6 ; Five tries before error
.LOOP:
    dec dh
    push ax
    mov ax, cx
    call l2hts
    pop ax
    mov ah, 2
    int 0x13
    jnc .DONE
    call reset_disk
    jc disk_error
    cmp dh, 0
    jne .LOOP
    mov si, disk_read_error
    call print_string
    jmp reboot
.DONE:
    popa
    ret

disk_error:
    mov si, fdisk_error
    call print_string
    jmp reboot

reset_disk:
    push ax
    push dx
    mov ax, 0
    mov dl, byte [boodev]
    stc
    int 0x13
    pop dx
    pop ax
    ret

l2hts:			; Calculate head, track and sector settings for int 13h		; IN: logical sector in AX, OUT: correct registers for int 13h
	push bx
	push ax

	mov bx, ax			; Save logical sector

	mov dx, 0			; First the sector
	div word [SectorsPerTrack]
	add dl, 01h			; Physical sectors start at 1
	mov cl, dl			; Sectors belong in CL for int 13h
	mov ax, bx

	mov dx, 0			; Now calculate the head
	div word [SectorsPerTrack]
	mov dx, 0
	div word [Sides]
	mov dh, dl			; Head/side
	mov ch, al			; Track

	pop ax
	pop bx

	mov dl, byte [bootdev]		; Set correct device

	ret

print_string:
    pusha
    mov ah, 0Eh
.REPEAT:
    lodsb
    cmp al, 0
    je .DONE
    int 10h
    jmp short .REPEAT
.DONE:
    popa
    ret

reboot:
    mov ax, 0
    int 16h
    mov ax, 0
    int 19h

no_kernel:
    mov si, msg_no_file
    call print_string
    jmp reboot

boodev db 0
bootMsg db 'Booting...', 0x0D, 0x0A, 0x00
msg_no_file db 'No files found!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0
disk_read_error db 'Could not read disk!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0
fdisk_error db 'Fatal disk error!', 0x0D, 0x0A, 'Press any key to reboot...', 0x0D, 0x0A, 0

file_start dw 0
file_length db 0

bootdev db 0

times 510-($-$$) db 0	; Pad remainder of boot sector with zeros
dw 0AA55h		; Boot signature (DO NOT CHANGE!)

buffer: